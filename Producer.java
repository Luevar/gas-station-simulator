package labs.luevar;

import java.util.Random;

class Producer<T> extends Thread {
    private Class<T> type;
    private CarQueue<T> carQueue;
    public static volatile double appearanceChance = 0.5;
    Random chance;

    public Producer(CarQueue<T> carQueue, Class<T> type) {
        this.type = type;
        this.carQueue = carQueue;
    }

    @Override
    public void run() {
        while (!interrupted()) {
            try {
                chance = new Random();
                Thread.sleep(50);
                if (appearanceChance > chance.nextDouble()) {
                    carQueue.appendNewCar(type);
                }
            } catch (InterruptedException IntEx) {
                System.out.println("Producer thread interrupted");
            } catch (Exception othersEx) {
                System.out.println("Can't create generic object for queue");
            }
        }
    }
}