package labs.luevar;

import java.util.InputMismatchException;

public class ThreadManager {
    private static Calculations<Car> calculations;

    public static boolean fillingStationIsBusy(int i) {
        return calculations.getConsumer().getStations().get(i).isBusy();
    }

    public void startCalculations() {
        Car sample = new Car();
        Class<?> type = sample.getClass();
        if (calculations == null) {
            calculations = new Calculations(type);
        }
    }

    public void stopCalculations() {
        calculations.interruptCalculations();
        calculations = null;
    }

    public void strParse(String str, int i) {
        if (str.matches("0(.|,)[0-9]{1,6} {0,5}")) {
            str = str.replace(',', '.');
            double num = Double.parseDouble(str);

            if (i == 1) {
                Producer.appearanceChance = num;
            } else {
                FillingStation.fillingSpeed = num;
            }
        } else throw new InputMismatchException();
    }
}













/*public static void  displayModelState() {
        while (GUI.calculationIsActive) {
            GUI.printQueueSize(CarQueue.queueSize);
            for (int i = 0; i < GasStation.FILLING_STATIONS_COUNT; i++) {
                GUI.showBusyStation(i, ThreadManager.fillingStationIsBusy(i));
            }
        }
    }*/

