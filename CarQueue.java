package labs.luevar;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CarQueue<T> extends ConcurrentLinkedQueue<T> {
    private volatile Queue<T> queue;

    private static int queueSize;

    public static int getQueueSize() {
        return queueSize;
    }

    public static void changeQueueSize() {
        CarQueue.queueSize--;
    }

    public Queue<T> getQueue() {
        return queue;
    }

    public CarQueue() {
        queue = new ConcurrentLinkedQueue<>();
    }

    public void appendNewCar(Class<T> cls) throws Exception{
        T newCar = cls.getDeclaredConstructor().newInstance();
        queue.add(newCar);
        queueSize++;
    }
}