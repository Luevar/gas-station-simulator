package labs.luevar;

public class Main {

    public static void main(String[] args) {
        GUI GUI = new GUI();
        GUI.setVisible(true);
        InitAll initAll = new InitAll(GUI);
        initAll.start();
    }
}
