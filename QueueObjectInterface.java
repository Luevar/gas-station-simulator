package labs.luevar;

public interface QueueObjectInterface {
    void setFuelAmount(int fuelAmount);
    int fuelDifferent();
    int getFuelMax();
    int getFuelAmount();
}
