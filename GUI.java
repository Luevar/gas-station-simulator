package labs.luevar;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.InputMismatchException;

public class GUI extends JFrame {
    private JPanel rootPanel;
    private JRadioButton JRadioButton1;
    private JRadioButton JRadioButton2;
    private JRadioButton JRadioButton3;
    private JRadioButton[] radioButtons = new JRadioButton[]{JRadioButton1, JRadioButton2, JRadioButton3};
    private JLabel QueueSizeLabel;
    private JButton startButton;
    private JButton stopButton;
    private JTextField textField1;
    private JTextField textField2;
    private JButton setValue1;
    private JButton setValue2;
    private JButton[] buttons = new JButton[]{setValue1, setValue2, startButton, stopButton};
    private ThreadManager threadManager;
    public static volatile boolean calculationIsActive = false;

    public void disableExcept(int i) {
        for (JButton button : buttons) {
            button.setEnabled(false);
        }
        buttons[i - 1].setEnabled(true);
    }

    public void enableAll() {
        for (JButton button : buttons) {
            button.setEnabled(true);
        }
    }

    public GUI() {
        setContentPane(rootPanel);
        setTitle("Gas station");
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        threadManager = new ThreadManager();
        stopButton.setEnabled(false);
        textField1.setEnabled(false);
        textField2.setEnabled(false);
        disableExcept(3);

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField1.setEnabled(true);
                textField2.setEnabled(true);
                enableAll();
                calculationIsActive = true;
                threadManager.startCalculations();
            }
        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculationIsActive = false;
                threadManager.stopCalculations();
                textField1.setEnabled(false);
                textField2.setEnabled(false);
                disableExcept(3);
            }
        });
        textField1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (!(e.getKeyChar() == 27)) {
                    textField2.setEnabled(false);
                    disableExcept(1);
                }
            }
        });
        textField2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (!(e.getKeyChar() == 27)) {
                    textField1.setEnabled(false);
                    disableExcept(2);
                }
            }
        });
        setValue1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    threadManager.strParse(textField1.getText(), 1);
                } catch (InputMismatchException inpMismatch) {
                    JOptionPane.showMessageDialog(null, "Wrong input.");
                }
                textField2.setEnabled(true);
                enableAll();
            }
        });
        setValue2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    threadManager.strParse(textField2.getText(), 2);
                } catch (InputMismatchException inpMismatch) {
                    JOptionPane.showMessageDialog(null, "Wrong input.");
                }
                textField1.setEnabled(true);
                enableAll();
            }
        });
    }

    public void displayModelState() {
        while (calculationIsActive) {
            QueueSizeLabel.setText(CarQueue.getQueueSize() + "");
            for (int i = 0; i < radioButtons.length; i++) {
                radioButtons[i].setSelected(ThreadManager.fillingStationIsBusy(i));
            }
        }
    }
}







  /*public static void showBusyStation(int i, boolean b) {
        radioButtons[i].setSelected(b);
    }

    public static void printQueueSize(int i) {
        QueueSizeLabel.setText(i + "");
    }*/
