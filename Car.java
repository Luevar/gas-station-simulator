package labs.luevar;

import java.util.Random;

public class Car implements QueueObjectInterface {
    private int fuelAmount;
    private int fuelMax;
    private static Random random = new Random();

    public int getFuelAmount() {
        return fuelAmount;
    }

    public int getFuelMax() {
        return fuelMax;
    }

    public int fuelDifferent() {
        return fuelMax - fuelAmount;

    }

    public void setFuelAmount(int fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public Car() {
        fuelMax = 40 + random.nextInt(80);
        fuelAmount = 10 + random.nextInt(fuelMax / 2);
//        refuelTime = fuelMax - fuelAmount;
    }
}