package labs.luevar;

import java.util.ArrayList;

public class GasStation<T extends QueueObjectInterface> {
    private ArrayList<FillingStation<T>> stations;
    public static final int FILLING_STATIONS_COUNT = 3;

    public ArrayList<FillingStation<T>> getStations() {
        return stations;
    }

    public GasStation(CarQueue<T> queue) {
        stations = new ArrayList<>();
        for (int i = 1; i <= FILLING_STATIONS_COUNT; i++) {
            FillingStation<T> fillingStation = new FillingStation<>(queue);
            stations.add(fillingStation);
            fillingStation.start();
        }
    }
}