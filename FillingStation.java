package labs.luevar;

public class FillingStation<T extends QueueObjectInterface> extends Thread {
    private T carPtr;
    private boolean busy = false;
    public static volatile double fillingSpeed = 0.5;
    private CarQueue<T> queue;

    public FillingStation(CarQueue<T> queue) {
        this.queue = queue;
    }

    public boolean isBusy() {
        return busy;
    }

    void fillCar(T car) throws InterruptedException {
        if (car == null) return;
        busy = true;
        this.carPtr = car;
        Thread.sleep( (int) (car.fuelDifferent() * 60 * (1 - fillingSpeed)));
        car.setFuelAmount(car.getFuelMax());
        this.carPtr = null;
        busy = false;
        Thread.sleep(500);
    }

    @Override
    public void run() {
        while (!interrupted()) {
            if (carPtr == null && queue.getQueue().size() != 0) {
                try {
                    T car = queue.getQueue().poll();
                    if (CarQueue.getQueueSize() > 0) CarQueue.changeQueueSize();
                    fillCar(car);
                } catch (InterruptedException interruptEx) {
                    System.out.println("FillingStationThread Interrupted!");
                }
            }
        }
    }
}












    /*void fillCar(T car) throws InterruptedException {
        if (carPtr != null) return;
        if (car == null) return;
        busy = true;
        this.carPtr = car;
        Thread.sleep( (int) (car.fuelDifferent() * 60 * (1 - fillingSpeed)));
        System.out.println("Fuel Amount: " + car.getFuelAmount());
        car.setFuelAmount(car.getFuelMax());
        System.out.println("Fuel Max: " + car.getFuelMax());
        System.out.println("Fuel Amount: " + car.getFuelAmount());
        System.out.println("");
        this.carPtr = null;
        busy = false;
        Thread.sleep(50);
    }*/