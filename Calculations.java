package labs.luevar;

class Calculations<T extends QueueObjectInterface> {
    private volatile CarQueue<T> carQueue;
    Producer<T> producer;
    Consumer<T> consumer;

    public Consumer<T> getConsumer() {
        return consumer;
    }

    Calculations(Class<T> type) {
        carQueue = new CarQueue<>();
        producer = new Producer(carQueue, type);
        consumer = new Consumer<>(carQueue);
        producer.start();
        consumer.start();
    }

  public void interruptCalculations() {
      producer.interrupt();
      for (var thread: consumer.getStations()) {
          thread.interrupt();
      }
  }
}





















/*  public void interruptCalculations() {
        for (var thread: consumer.getThreads()) {
            thread.stop();
        }
        consumer.stop();
        producer.stop();
    }*/