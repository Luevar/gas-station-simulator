package labs.luevar;

import java.util.concurrent.locks.ReentrantLock;

public class InitAll extends Thread {
    ReentrantLock locker;
    GUI GUI;

    public InitAll(GUI GUI) {
        this.GUI = GUI;
        locker = new ReentrantLock();
    }

    @Override
    public void run() {
        locker.lock();
        try {
            do {
                sleep(500);
            } while (!GUI.calculationIsActive);
            GUI.displayModelState();
        } catch (InterruptedException intEx) {
            System.out.println("InitAll thread interrupted!");
        } finally {
            locker.unlock();
        }
    }
}


//    Condition condition;

/*
    public void run() {
        locker.lock();
        try {
            while (!(GUI.calculationIsActive)) {
                condition.await();
            }
            System.out.println("LOCK ESCAPED");
            condition.signalAll();
            GUI.displayModelState();
        } catch (InterruptedException exs) {
            System.out.println("Sheet");
        } finally {
            locker.unlock();
        }
    }*/
