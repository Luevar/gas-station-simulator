package labs.luevar;

import java.util.ArrayList;

class Consumer<T extends QueueObjectInterface> extends Thread {
    GasStation<T> gasStation;
    private CarQueue<T> queue;

    public ArrayList<FillingStation<T>> getStations() {
        return gasStation.getStations();
    }

    public Consumer(CarQueue<T> queue) {
        this.queue = queue;
    }

    public void run() {
        gasStation = new GasStation<>(queue);
    }
}
